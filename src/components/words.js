var PROGRAMING_LANG = [
	"gilbert",
	"haekal",
	"ingram",
	"dimas",
	"abu",
	"nurul",
	"fio",
	"naufal",
	"feri",
	"andre",
	"heince",
	"ginan",
	"jimmy",
	"jippie"
];

function randomWord() {
	return PROGRAMING_LANG[Math.floor(Math.random() * PROGRAMING_LANG.length)];
}

export { randomWord };
